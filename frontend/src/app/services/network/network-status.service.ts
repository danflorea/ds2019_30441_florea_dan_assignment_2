import { Injectable } from '@angular/core';
import { fromEvent, Observable, Observer, merge } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class NetworkStatusService {

  constructor() { }

  public isClientOnline()
  {
    return merge<boolean>
    (
      fromEvent(window, 'offline').pipe(map(()=>false)),
      fromEvent(window, 'online').pipe(map(()=>true)),
      new Observable((sub: Observer<boolean>) => 
      {
        sub.next(navigator.onLine);
        sub.complete();
      })
    )
  }
}
