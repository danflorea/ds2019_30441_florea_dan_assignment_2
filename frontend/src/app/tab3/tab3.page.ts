import { Component } from '@angular/core';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})

export class Tab3Page {

  public playersList: Player[] = [];
  scoreIncrement: number = 1;
  controller = document.querySelector('ion-alert-controller');

  constructor(private alertCtrl: AlertController) {}

  ngOnInit(){
    this.playersList.push(new Player("Johnny Guitar", 0));
    this.playersList.push(new Player("Otter von Bismarck", 0));
    console.log("it's happening");
  }

  decrementScore(player: Player){
    player.currentScore -= this.scoreIncrement;
    console.log(player.currentScore);
  }

  incrementScore(player: Player){
    player.currentScore += this.scoreIncrement;
    console.log(player.currentScore);
  }

  addNewPlayer(){
    console.log(this.scoreIncrement);
    this.alertCtrl.create({
      header: 'Add a new player, please',
      inputs: [
        {
          name: 'playername',
          placeholder: 'Player Name'
        }
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Add player',
          handler: data => {
            var newPlayer = new Player (data.playername, 0);
            this.playersList.push(newPlayer);
          }
        }
      ]
    }).then(alert => {
      alert.present();
    });
    console.log("Here are the current players:\n");
    console.log(this.playersList);
  }

}

class Player{

  constructor (public username: string, public currentScore: number){ }

}
