export class MedUser {

    public uid: number;
    public name: string;
    public gender: string;
    public address: string;
    public birthdate: string;
    public username: string;
    public password: string;
    public role: string;

    constructor(
        name?:string,
        birthdate?: string,
        address?: string,
        username?: string,
        password?: string,
        role?: string,
        gender?: string
    ) { 
        this.name = name;
        this.birthdate = birthdate;
        this.address = address;
        this.username = username;
        this.password = password;
        this.role = role;
        this.gender = gender;
    }

   

    public toStringMin () {
        return this.name.toString() + " | " + this.birthdate + " | " + this.address;
    }
}