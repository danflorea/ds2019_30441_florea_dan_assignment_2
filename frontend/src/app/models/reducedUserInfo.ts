export class ReducedUserInfo {
    
    constructor (
        public ruid: number,
        public username: string,
        public password: string
    ) {}

}