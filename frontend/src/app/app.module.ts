import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { Camera } from '@ionic-native/camera/ngx';
import { HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { httpInterceptorProviders } from './http-interceptors';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { TabsPageModule } from './tabs/tabs.module';
import { LoginFormModule } from './components/login-form/login-form.module';
import { LoginPageModule } from './auth/login/login.module';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule, 
    HttpClientModule,
    IonicModule.forRoot({
      mode: "md"
    }), 
    AppRoutingModule,
    TabsPageModule,
    LoginPageModule,
    IonicStorageModule.forRoot()],
  providers: [
    httpInterceptorProviders,
    QRScanner,
    StatusBar,
    SplashScreen,
    Camera,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
