import { Component, OnInit } from '@angular/core';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { ToastController, Platform } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {  

  constructor(private qrScanner: QRScanner) { }
  
  ngOnInit() {

  }


  scanQRCode(){
    this.qrScanner.useBackCamera();
    
    const ionApp = <HTMLElement>document.getElementsByTagName('ion-app')[0];
    var scanSub;

    this.qrScanner.prepare()
      .then((status: QRScannerStatus) => {
        if (status.authorized) {
          console.log("Permission granted");

          scanSub = this.qrScanner.scan().subscribe((text: string) => {
            console.log(text);
            window.open(text);

            this.qrScanner.hide();
            scanSub.unsubscribe();
            ionApp.style.display = 'block';
        });

        this.qrScanner.show();
        ionApp.style.display = 'none';

        } else if (status.denied) {
          console.log("Permission denied forever");
        } else {
          console.log("Permission denied temporarily")
        }
        
    })
    .catch((e: any) => console.log('Error is', e));

    
  }

}
