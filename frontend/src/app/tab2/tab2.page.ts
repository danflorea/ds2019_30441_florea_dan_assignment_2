import { Component } from '@angular/core';
import { Observable, interval, throwError, of } from 'rxjs'
import { HttpClient } from '@angular/common/http';
import { Network } from '@ionic-native/network/ngx'
import { ConfigService } from '../services/config/config.service';
import { retryWhen, flatMap } from 'rxjs/operators';
import { NetworkStatusService } from '../services/network/network-status.service';
import { ServerStatusService } from '../services/network/server-status.service';
import { MedUser } from '../models/medUser';
import { ReducedUserInfo } from '../models/reducedUserInfo';
import { Medication } from '../models/medication';
import { CurrentUser } from '../models/currentUser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {

  constructor(
    private configService: ConfigService, 
    private router: Router, 
    private serverStatusService: ServerStatusService) { }

  backend = "localhost:8080";
  url = "meds";
  medsList: Array<Medication> = [];
  frm = [];
  isLoggedIn = false;

  nameInput: string;
  side_effectsInput: string;

  uMid: number;
  uName: string;
  uSide_effects: string;

  updateMedList() {
    this.configService.getConfig(this.url).subscribe(u => {
      this.medsList = u;
    });
  }

  private async delay(ms: number) {
    return new Promise(resolve => setTimeout(() => {
      resolve('delayed');
    }, ms));
  }

  ngOnInit() {
    if (CurrentUser.getInstance().role === "DOCTOR") {
      this.isLoggedIn = true;
    } else {
      this.router.navigate(['/login']);
    }
    this.updateMedList();
  }

  async deleteMedication(med: Medication) 
  {
    console.log(med);
    this.configService.deleteConfig(med.mid, this.url).subscribe();
    console.log(this.serverStatusService.status);
    await this.delay(50);
    this.updateMedList();
  }

  async addMedication() 
  {
    let u = new Medication(
      this.nameInput,
      this.side_effectsInput
    );
    this.configService.postConfig(u, this.url).subscribe(mu => {
      console.log("added med ", mu);
    });
    this.medsList.push(u);
    await this.delay(50);
    this.updateMedList();
  }

  updateMedicationCardInitializer(med: Medication) 
  {
    console.log(med);
    this.uName = med.name;
    this.uMid = med.mid;
    this.uSide_effects = med.side_effects;

    document.getElementById("updateMedCard").scrollIntoView();
  }

  async updateMedication() {
    let newUser = new Medication(
      this.uName,
      this.uSide_effects
    );
    newUser.mid = this.uMid;
    console.log("user", newUser);
    this.configService.putConfig(newUser, this.url).subscribe();
    await this.delay(50);
    this.updateMedList();
  }
}
