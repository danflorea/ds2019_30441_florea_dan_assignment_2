import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CaregiverPage } from './caregiver.page';

describe('CaregiverPage', () => {
  let component: CaregiverPage;
  let fixture: ComponentFixture<CaregiverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CaregiverPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CaregiverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
