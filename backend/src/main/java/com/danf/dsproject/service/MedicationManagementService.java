package com.danf.dsproject.service;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.entities.Medication;
import com.danf.dsproject.persistence.jpa.JpaRepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicationManagementService {

    private final JpaRepositoryFactory factory;

    public void delete (Medication med) {
        factory.createMedicationRepository().remove(med);
    }

    public Optional<Medication> findMedicationById (Integer id) {
        return this.factory.createMedicationRepository().findById(id);
    }

    public List<Medication> findAll () {
        return this.factory.createMedicationRepository().findAll();
    }

    public Medication save (Medication med) {
        return this.factory.createMedicationRepository().save(med);
    }

}
