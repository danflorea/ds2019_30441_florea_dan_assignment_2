package com.danf.dsproject.persistence.jpa;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.receiver.PatientData;
import org.springframework.data.repository.Repository;

import java.util.List;
import java.util.Optional;

public interface JpaPatientDataRepository extends Repository<PatientData, Integer> {

    void delete(PatientData patientData);

    default void remove(PatientData patientData) {
        delete(patientData);
    }

    List<PatientData> findAll();

    PatientData save(PatientData patientData);

    Optional<PatientData> findById (Integer id);

}
