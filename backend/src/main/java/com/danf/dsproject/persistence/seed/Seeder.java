package com.danf.dsproject.persistence.seed;

import com.danf.dsproject.entities.MedUser;
import com.danf.dsproject.entities.UserRoles;
import com.danf.dsproject.persistence.jpa.JpaRepositoryFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
@RequiredArgsConstructor
public class Seeder implements CommandLineRunner {

    private final JpaRepositoryFactory jpaRepositoryFactory;

    @Override
    public void run(String... args) throws Exception {
//        MedUser u1 = new MedUser("Andrei", "male", "01/01/2001", "yes", "aa", "aa", "PATIENT");
//        MedUser u2 = new MedUser("Karl", "male", "01/01/2002", "yes", "bb", "aa", "PATIENT");
//
//        List<MedUser> patients = new ArrayList<>();
//        patients.add(u1);
//        patients.add(u2);
//
//        MedUser c = new MedUser("Kikero", "male", "03/03/1978", "fgds", "cc", "aa", "CAREGIVER", patients);
//
//        this.jpaRepositoryFactory.createMedUserRepository().save(u1);
//        this.jpaRepositoryFactory.createMedUserRepository().save(u2);
//        this.jpaRepositoryFactory.createMedUserRepository().save(c);
    }
}
