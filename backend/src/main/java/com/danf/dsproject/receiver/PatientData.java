package com.danf.dsproject.receiver;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table
public class PatientData {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private String patient_id;
    private String activity;
    private Long startTime;
    private Long endTime;

    public PatientData(String patient_id, String activity, Long startTime, Long endTime) {
        this.patient_id = patient_id;
        this.activity = activity;
        this.startTime = startTime;
        this.endTime = endTime;
    }

    public String toString()
    {
        return "\n{\n patient_id : " + this.getPatient_id() +
                "\n activity : " + this.getActivity() +
                "\n start : " + this.getStartTime().toString() +
                "\n end : " + this.getEndTime().toString() +
                "\n}\n";
    }
}
