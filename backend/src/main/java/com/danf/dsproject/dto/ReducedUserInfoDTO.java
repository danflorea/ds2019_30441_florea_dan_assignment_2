package com.danf.dsproject.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReducedUserInfoDTO {

    private Integer ruid;
    private String username;
    private String password;

}
