package com.danf.dsproject.dto;

import com.danf.dsproject.entities.Medication;
import lombok.Data;

@Data
public class MedicationDTO {

    private Integer mid;
    private String name;
    private String side_effects;

    public static MedicationDTO ofEntity(Medication med) {
        MedicationDTO medicationDTO = new MedicationDTO();
        medicationDTO.setName(med.getName());
        medicationDTO.setSide_effects(med.getSide_effects());
        return medicationDTO;
    }
}
