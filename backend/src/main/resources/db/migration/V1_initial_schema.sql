CREATE TABLE IF NOT EXISTS mediuser
(
    uid       serial PRIMARY KEY,
    fullname  VARCHAR(32) NOT NULL,
    gender    VARCHAR(8)  NOT NULL,
    birthdate VARCHAR(32) NOT NULL,
    address   VARCHAR(64) NOT NULL,
    username  VARCHAR(16) NOT NULL,
    pass      VARCHAR(16) NOT NULL,
    urole     VARCHAR(16) NOT NULL
)