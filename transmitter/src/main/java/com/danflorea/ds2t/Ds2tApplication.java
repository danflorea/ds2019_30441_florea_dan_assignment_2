package com.danflorea.ds2t;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Ds2tApplication {

	public static void main(String[] args) {
		SpringApplication.run(Ds2tApplication.class, args);
	}

}
