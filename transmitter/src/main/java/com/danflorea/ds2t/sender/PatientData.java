package com.danflorea.ds2t.sender;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PatientData {

    private String patient_id;
    private String activity;
    private Long startTime;
    private Long endTime;

    public String toString()
    {
        return "{\n \"patient_id\" : \"" + this.getPatient_id() +
                "\"\n \"activity\" : \"" + this.getActivity() +
                "\"\n \"start\" : \"" + this.getStartTime().toString() +
                "\"\n \"end\" : \"" + this.getEndTime().toString() +
                "\"\n}\n";
    }
}
